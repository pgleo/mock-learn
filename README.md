### powerMockito单元测试准备
1 在需要执行单测的类上注解@RunWith(PowerMockRunner.class)
2 对于需要mock私有方法的需要注解@PrepareForTest(FooServiceImpl.class)

### 私有方法mock
com.mock.test.service.impl.FooServiceImplTest.private_method()

### web controller mock
com.mock.test.controller

### InjectMocks和Mock区别
InjectMocks创建该类的一个实例，并将使用@Mock（或@Spy）注释创建的模仿注入到此实例中。此处生成的是实例，不能直接当做mock对象使用
Mock 仅mock该对象本身，不会对里面的属性值做处理，默认是NULL
请注意，您必须使用@RunWith(MockitoJUnitRunner.class)或Mockito.initMocks(this)来初始化这些模拟并注入它们。

### doReturn和thenReturn区别
doReturn 不会当实际方法本身做调用
thenReturn 会执行实际的方法，但是返回结果会被mock值替换

### any() 和anyString() 区别
any()表示任意对象值，包括null对象
anyString()入参值必须是string类型的实例，但是null不是string类型实例，所以当入参可能为null时，对于anyString可以使用any来替换，负责可能匹配不上导致执行原生方法



