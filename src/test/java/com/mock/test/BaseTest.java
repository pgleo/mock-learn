package com.mock.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public abstract class BaseTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Before
    public void before() throws Exception {
        setUp();
    }
    public void setUp() throws Exception{

    }

    @After
    public void after() throws Exception {
        shutdown();
    }
    public void shutdown() throws Exception{}
}
