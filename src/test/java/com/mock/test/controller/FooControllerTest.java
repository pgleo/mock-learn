package com.mock.test.controller;

import com.mock.test.BaseTest;
import com.mock.test.Model.Foo;
import com.mock.test.dao.FooDao;
import com.mock.test.service.impl.FooServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FooControllerTest extends BaseTest {

    @Mock
    private FooServiceImpl fooService;

    @InjectMocks
    FooController fooController;

    @Mock
    private FooDao fooDao;

    private MockMvc mvc;

    @Override
    public void setUp() throws Exception {
        //初始化
        MockitoAnnotations.initMocks(this);
        //构建mvc环境
        mvc = MockMvcBuilders.standaloneSetup(fooController).build();

        MemberModifier.field(FooServiceImpl.class,"fooDao").set(fooService,fooDao);
    }

    @Test
    public void foo() throws Exception {
        final Foo aa = new Foo("aa", 1);
        PowerMockito.doReturn(aa).when(fooService).query(anyString());
        this.mvc.perform(get("/query/zhang")).andExpect(status().isOk()).andExpect(content().json(aa.toString()));
    }

    @Test
    public void add() throws Exception {
        final Foo aa = new Foo("aa1", 1);
        PowerMockito.when(fooService.add(any(Foo.class))).thenCallRealMethod();
        PowerMockito.doReturn(3).when(fooDao).add(argThat(foo -> "aa1".equals(foo.getName())));
        this.mvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(aa.toString())).andExpect(status().isOk()).andExpect(content().json("3"));
    }
}