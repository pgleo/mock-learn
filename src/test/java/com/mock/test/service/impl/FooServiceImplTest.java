package com.mock.test.service.impl;

import com.mock.test.BaseTest;
import com.mock.test.Model.Foo;
import com.mock.test.dao.FooDao;
import com.mock.test.service.CallBackFunction;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.powermock.api.mockito.PowerMockito.doReturn;

@PrepareForTest(FooServiceImpl.class)
public class FooServiceImplTest extends BaseTest {

    //@InjectMocks 会将该类中需要注入的属性加入进去
    @InjectMocks
    private FooServiceImpl fooService ;
    @Mock
    private FooDao fooDao;

    @Override
    public void setUp() throws Exception {
        fooService = PowerMockito.spy(fooService);
        //mock 修改私有属性
        // fooService没有使用@InjectMocks ,则需要打开下面代码手动修改属性
//        MemberModifier.field(FooServiceImpl.class,"fooDao").set(fooService,fooDao);
    }
    @Test
    public void private_method() throws Exception {
        final Foo a = new Foo("aa", 1);

        //模拟私有方法的返回值
        doReturn(false).when(fooService, "check", Mockito.argThat(new ArgumentMatcher<Foo>(){
            @Override
            public boolean matches(Foo foo) {
                return foo.getName().equals("aa");
            }
        }));
        doReturn(true).when(fooService, "check", Mockito.argThat(new ArgumentMatcher<Foo>(){
            @Override
            public boolean matches(Foo foo) {
                return !foo.getName().equals("aa");
            }
        }));
        PowerMockito.when(fooDao.add(any())).thenReturn(2);

        assert fooService.add(a)==2;
    }

    @Test
    public void add_sucess() {
        PowerMockito.when(fooDao.add(any())).thenReturn(2);
        assert fooService.add(new Foo("aa1",1))==2;
    }

    @Test
    public void add_fail() throws Exception{
        PowerMockito.when(fooDao.add(any())).thenReturn(2);
        assert fooService.add(new Foo("aa",1))==0;
    }

    @Test
    public void test_callBackMrthod(){
//        PowerMockito.when(fooService).callBackMrthod(anyString(),any());
        final CallBackFunction callBackFunction = PowerMockito.mock(CallBackFunction.class);
        fooService.callBackMrthod("aa",callBackFunction);
        Mockito.verify(callBackFunction).onSucess("aa");
        Mockito.verify(callBackFunction, Mockito.times(1)).onSucess("aa");
        doReturn("fail").when(callBackFunction).onFail(argThat(e->"bb".equals(e)));
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("异常测试");
        fooService.callBackMrthod("bb",callBackFunction);
        Mockito.verify(callBackFunction, Mockito.times(1)).onFail("bba");
    }

    @Test
    public void test_callBackMrthod1(){
        final CallBackFunction callBackFunction = PowerMockito.mock(CallBackFunction.class);
        doReturn("fail").when(callBackFunction).onFail(argThat(e->"bb".equals(e)));
        fooService.callBackMrthod("bba",callBackFunction);
        Mockito.verify(callBackFunction).onFail("bba");
    }

    @Test
    public void test_exception(){
        final CallBackFunction callBackFunction = PowerMockito.mock(CallBackFunction.class);
        doReturn("fail").when(callBackFunction).onFail(argThat(e->"bb".equals(e)));
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("异常测试");
        fooService.callBackMrthod("bb",callBackFunction);
    }

    @Test
    public void test_del(){
        doReturn(0).when(fooDao).add(any(Foo.class));
        assert fooService.del(null)==0;
    }
}