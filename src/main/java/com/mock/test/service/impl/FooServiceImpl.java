package com.mock.test.service.impl;

import com.mock.test.Model.Foo;
import com.mock.test.dao.FooDao;
import com.mock.test.service.CallBackFunction;
import com.mock.test.service.FooService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FooServiceImpl implements FooService {

    @Resource
    FooDao fooDao;

    @Override
    public int add(Foo foo) {
        if (!check(foo)){
            return fooDao.add(foo);
        }
        return 0;
    }

    private boolean check(Foo foo){
        System.out.println("-----aaaa----");
        return foo.getName().equals("aa");
    }

    @Override
    public int del(String name) {
        return fooDao.remove( name);
    }

    @Override
    public void callBackMrthod(String name,CallBackFunction callBackFunction) {
        if (name.equals("aa")) {
            callBackFunction.onSucess("aa");
        }else{
            final String failRs = callBackFunction.onFail(name);
            if ("fail".equals(failRs)) {
                throw new IllegalStateException("异常测试");
            }
        }
    }

    @Override
    public Foo query(String name) {
        return fooDao.query(name);
    }

}
