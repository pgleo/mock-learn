package com.mock.test.service;

public interface CallBackFunction {

    public void onSucess(String name);

    public String onFail(String name);
}
