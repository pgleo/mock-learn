package com.mock.test.service;

import com.mock.test.Model.Foo;

public interface FooService {

    public int add(Foo foo);

    public int del(String name);

    public void callBackMrthod(String name,CallBackFunction callBackFunction);

    public Foo query(String name);
}
