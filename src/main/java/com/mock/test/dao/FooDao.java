package com.mock.test.dao;

import com.mock.test.Model.Foo;

public interface FooDao {

    public int add(Foo foo);

    int remove(String name);

    Foo query(String name);
}
