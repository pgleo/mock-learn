package com.mock.test.controller;

import com.mock.test.Model.Foo;
import com.mock.test.service.FooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class FooController {

    @Autowired
    private FooService fooService;

    @GetMapping("/query/{name}")
    public Foo foo(@PathVariable String name){
        return fooService.query(name);
    }

    @PostMapping("/add")
    public int foo(@RequestBody Foo foo){
        return fooService.add(foo);
    }
}
